# hnvue-demo

> A project to demo the power of Vue CLI, Components, Router and more.

## Build Setup

If you have Yarn, use Yarn:

``` bash
yarn
```

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:9000
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```
