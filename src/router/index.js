import Vue from 'vue'
import Router from 'vue-router'
import HomeView from '@/components/HomeView'
import SimpleKanbanView from '@/components/kanban/SimpleKanbanView'
import PersonView from '@/components/unicomplex/PersonView'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: HomeView
    },
    {
      path: '/kanban',
      name: 'Kanban',
      component: SimpleKanbanView
    },
    {
      path: '/person',
      name: 'Person',
      component: PersonView
    }
  ]
})
